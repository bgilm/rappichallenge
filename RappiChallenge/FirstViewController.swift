//
//  FirstViewController.swift
//  RappiChallenge
//
//  Created by Benjamin on 2/10/17.
//  Copyright © 2017 Benjamin. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITableViewDataSource{
    
    

    final let urlString = "https://itunes.apple.com/us/rss/topfreeapplications/limit=20/json"
    @IBOutlet weak var tableTopFree: UITableView!
    
    var nameArray = [String]()
    var categoryArray = [String]()
    var summaryArray = [String]()
    var imageArray = [String]()
    var devsArray = [String]()
    
    var selectedName:String?
    var selectedDev:String?
    var selectedSummary:String?
    var selectedImgUrl:NSURL?
    
    var imageCache = [NSURL: UIImage]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.downloadJsonWithTask()
    }
    func downloadJson(){
        let url = NSURL(string: urlString)
        URLSession.shared.dataTask(with: (url as? URL)!, completionHandler: {(data, response, error) ->
            Void in
            
            if let jsonObj = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary{
                //path to traverse the JsonObj example
                //print(jsonObj!.value(forKeyPath: "feed.entry.im:name.label"))
                if let entryArray = jsonObj!.value(forKeyPath: "feed.entry") as? NSArray{
                    for entry in entryArray{
                        if let entryDict = entry as? NSDictionary{
                            if let name = entryDict.value(forKeyPath: "im:name.label"){
                                self.nameArray.append(name as! String)
                            }
                            if let name = entryDict.value(forKeyPath: "category.attributes.term"){
                                self.categoryArray.append(name as! String)
                            }
                            if let name = entryDict.value(forKeyPath: "summary.label"){
                                self.summaryArray.append(name as! String)
                            }
                            if let name = entryDict.value(forKeyPath: "rights.label"){
                                self.devsArray.append(name as! String)
                            }
                            if let name = entryDict.value(forKeyPath: "im:image.label") as? NSArray{
                                //print(name[0]) 3 sizes per icon
                                //0 - 53 height
                                //1 - 75 height
                                //2 - 100 height
                                self.imageArray.append(name[2] as! String)
                            }
                            OperationQueue.main.addOperation ({
                                self.tableTopFree.reloadData()
                            })
                            
                        }
                        
                    }
                }
      
            }
            
        }).resume()
    }
    
    func downloadJsonWithTask(){
        let url = NSURL(string: urlString)
        
        var downloadTask = URLRequest(url: (url as? URL)!, cachePolicy:URLRequest.CachePolicy.returnCacheDataElseLoad, timeoutInterval: 20)
        downloadTask.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: downloadTask, completionHandler: {(data,response,error)->
            Void in
            
            if let jsonObj = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary{
                //path to traverse the JsonObj example
                //print(jsonObj!.value(forKeyPath: "feed.entry.im:name.label"))
                if let entryArray = jsonObj!.value(forKeyPath: "feed.entry") as? NSArray{
                    for entry in entryArray{
                        if let entryDict = entry as? NSDictionary{
                            if let name = entryDict.value(forKeyPath: "im:name.label"){
                                self.nameArray.append(name as! String)
                            }
                            if let name = entryDict.value(forKeyPath: "category.attributes.term"){
                                self.categoryArray.append(name as! String)
                            }
                            if let name = entryDict.value(forKeyPath: "summary.label"){
                                self.summaryArray.append(name as! String)
                            }
                            if let name = entryDict.value(forKeyPath: "rights.label"){
                                self.devsArray.append(name as! String)
                            }
                            if let name = entryDict.value(forKeyPath: "im:image.label") as? NSArray{
                                //print(name[0]) 3 sizes per icon
                                //0 - 53 height
                                //1 - 75 height
                                //2 - 100 height
                                self.imageArray.append(name[2] as! String)
                            }
                            OperationQueue.main.addOperation ({
                                self.tableTopFree.reloadData()
                            })
                            
                        }
                        
                    }
                }
                
            }

        }).resume()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if let index = self.tableTopFree.indexPathForSelectedRow{
            self.tableTopFree.deselectRow(at: index, animated: true)
            
            
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath: NSIndexPath){
        selectedName = nameArray[didSelectRowAtIndexPath.row]
        selectedDev = devsArray[didSelectRowAtIndexPath.row]
        selectedSummary = summaryArray[didSelectRowAtIndexPath.row]
        selectedImgUrl = NSURL(string: imageArray[didSelectRowAtIndexPath.row])
        performSegue(withIdentifier: "detailSegue", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            let dest = segue.destination as! CellDetailViewController
            dest.appName = selectedName
            dest.devsName = selectedDev!
            dest.summaryName = selectedSummary!
            dest.appIconUrl = selectedImgUrl!
            dest.imageCache = self.imageCache
        
            // Missing Image pass due to cache handling
        
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let celda = tableView.dequeueReusableCell(withIdentifier: "celda") as! TableViewCell
        celda.name.text = nameArray[indexPath.row]
        celda.category.text = categoryArray[indexPath.row]
        
        let imgURL = NSURL(string: imageArray[indexPath.row])
        
        if(imgURL != nil){
            if let image = imageCache[imgURL!]{
                celda.icon.image = image
            }else{
                let data = NSData (contentsOf: (imgURL as? URL)!)
                imageCache[imgURL!] = UIImage(data: data as! Data)
                if(UIImage(data: data as! Data) != nil){
                     celda.icon.image = UIImage(data: data as! Data)
                }else{
                    
                }
               
            }
            
        }
        
        return celda
    }
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

