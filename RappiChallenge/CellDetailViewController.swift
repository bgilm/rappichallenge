//
//  CellDetailViewController.swift
//  RappiChallenge
//
//  Created by Benjamin on 2/12/17.
//  Copyright © 2017 Benjamin. All rights reserved.
//

import UIKit

class CellDetailViewController: UIViewController {
    var appName: String?
    var devsName: String=""
    var summaryName: String=""
    var appIconUrl: NSURL?
    
    var imageCache = [NSURL: UIImage]()
    @IBOutlet weak var devs: UILabel!
    @IBOutlet weak var summary: UILabel!
    @IBOutlet weak var iconImg: UIImageView!
    
    @IBOutlet weak var appNameLabel: UILabel!
 
    override func viewWillAppear(_ animated: Bool) {
        
        appNameLabel.text = appName
        summary.text = summaryName
        devs.text = devsName
        
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        appNameLabel.text = appName
        summary.text = summaryName
        devs.text = devsName
        
        
            if let image = imageCache[appIconUrl!]{
                iconImg.image = image
            }else{
                let data = NSData (contentsOf: (appIconUrl as? URL)!)
                imageCache[appIconUrl!] = UIImage(data: data as! Data)
                if(UIImage(data: data as! Data) != nil){
                    iconImg.image = UIImage(data: data as! Data)
                }else{
                    
                }
                
            }
            
        
        
       

        // Do any additional setup after loading the view.
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
