//
//  TableViewCell.swift
//  RappiChallenge
//
//  Created by Benjamin on 2/11/17.
//  Copyright © 2017 Benjamin. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        icon.layer.cornerRadius = 10
        icon.clipsToBounds = true
        
    }
    @IBOutlet weak var category: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
